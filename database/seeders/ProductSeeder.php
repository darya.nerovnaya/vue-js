<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{

    public function run()
    {
        $array_products = [
            [
                'name' => 'Набір "Панда" (бірюзовий) 3 речі',
                'price' => '1063',
                'size' => '56',
                'photo' => 'https://lari.ua/upload/iblock/1a6/1a6cfef4076dcc3126f05fdcdc020e94.jpg',
                'description' => 'Утеплювач - синтепон, для хлопчика, обробка - вишивка',
            ],
            [
                'name' => 'Набір "Princess" (д/с) - 3 речі',
                'price' => '1205',
                'size' => '56',
                'photo' => 'https://lari.ua/upload/iblock/d1f/d1f34d6065ae7799d4f9b3ff06575a59.JPG',
                'description' => 'Утеплювач - синтепон, для дівчинки, тканина - велюр',
            ],
            [
                'name' => 'Набір "Prince" (д/с) - 3 речі',
                'price' => '1205',
                'size' => '56',
                'photo' => 'https://lari.ua/upload/iblock/8a4/8a4a3ffed6a29901939445992a2b1d85.JPG',
                'description' => 'Утеплювач - синтепон, для хлопчика, тканина - велюр',
            ],
            [
                'name' => 'Набір "Маленькі стиляжки" 3 речі',
                'price' => '1205',
                'size' => '56',
                'photo' => 'https://lari.ua/upload/iblock/6b5/6b5454a0b3cb502e42d4566a87c2b54f.jpg',
                'description' => 'Утеплювач - синтепон, для хлопчика або дівчинки, тканина - велюр',
            ],
            [
                'name' => 'Набір "Зефірка" (кремовий) 3 речі',
                'price' => '990',
                'size' => '56',
                'photo' => 'https://lari.ua/upload/iblock/6b5/6b5454a0b3cb502e42d4566a87c2b54f.jpg',
                'description' => 'Утеплювач - синтепон, для хлопчика або дівчинки, тканина - плюш',
            ],
            [
                'name' => 'Набір "Панда" (молочний шоколад) 3 речі',
                'price' => '1328',
                'size' => '56',
                'photo' => 'https://lari.ua/upload/iblock/aad/aad0798f095fe20e10663cc477228384.jpg',
                'description' => 'Утеплювач - синтепон, для хлопчика або дівчинки, тканина - велюр, обробка - вишивка',
            ],
            [
                'name' => 'Набір "Панда" (ментоловий) 3 речі',
                'price' => '1328',
                'size' => '56',
                'photo' => 'https://lari.ua/upload/iblock/89a/89a160351c0c4a2f727d05274f229d15.jpg',
                'description' => 'Утеплювач - синтепон, для хлопчика або дівчинки, тканина - велюр, обробка - вишивка',
            ],
            [
                'name' => 'Набір "Панда" (лимонний) 3 речі',
                'price' => '1328',
                'size' => '56',
                'photo' => 'https://lari.ua/upload/iblock/3d8/3d824926677f7983711e997c92318566.jpg',
                'description' => 'Утеплювач - синтепон, для хлопчика або дівчинки, тканина - велюр, обробка - вишивка',
            ],
            [
                'name' => 'Набір "Панда" (кремовий) 3 речі',
                'price' => '1328',
                'size' => '56',
                'photo' => 'https://lari.ua/upload/iblock/2af/2af4e55f110dbf1802b6dac2b230c868.jpg',
                'description' => 'Утеплювач - синтепон, для хлопчика або дівчинки, тканина - велюр, обробка - вишивка',
            ],
            [
                'name' => 'Набір "Панда" (голубий меланж) 3 речі',
                'price' => '1328',
                'size' => '56',
                'photo' => 'https://lari.ua/upload/iblock/225/225ef6af28457ed606fb030928cfbe4e.jpg',
                'description' => 'Утеплювач - синтепон, для хлопчика, тканина - велюр, обробка - вишивка',
            ],
            [
                'name' => 'Набір "Зефірка" (рожевий) 3 речі',
                'price' => '1328',
                'size' => '56',
                'photo' => 'https://lari.ua/upload/iblock/198/198290e792adc06b52bc9434fd4add21.jpg',
                'description' => 'Утеплювач - синтепон, для хлопчика, тканина - плюш',
            ],
            [
                'name' => 'Набір "Чарівність" (крем/слива) 3 речі',
                'price' => '1328',
                'size' => '56',
                'photo' => 'https://lari.ua/upload/iblock/bff/bffbd4124ab5a1cac02180e724f6417a.jpg',
                'description' => 'Утеплювач - синтепон, для хлопчика, тканина - велюр, обробка - атласна вставка, вишивка',
            ],
            [
                'name' => 'Набір "Змійка" (голубий) 3 речі',
                'price' => '1328',
                'size' => '56',
                'photo' => 'https://lari.ua/upload/iblock/cd5/cd51fc873464ca64ad93f6d551ad1aa2.jpg',
                'description' => 'Утеплювач - синтепон, для хлопчика, тканина - вязане полотно',
            ],
        ];

        foreach($array_products as $array_product) {
           DB::table('products')->insert($array_product);
        }
     }
}

