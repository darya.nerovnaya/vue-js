<?php

namespace App\Http\Controllers;


use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthenticationController extends Controller
{
    public function createAccount(Request $request)
    {
        $post_data = $request->validate([
            'name'=>'required|string',
            'email'=>'required|string|email|unique:users',
            'password'=>'required|min:8'
        ]);

        $user = User::create([
            'name' => $post_data['name'],
            'email' => $post_data['email'],
            'password' => Hash::make($post_data['password']),
        ]);

        $token = $user->createToken('authToken')->plainTextToken;

        Auth::attempt([
            'email' => $user->email,
            'password' => $user->password,
        ]);
        return response()->json([
            'token' => $token,
            'user'  => $user,
        ]);
    }

    public function signin(Request $request)
    {
        $attr = $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string|min:6'
        ]);
        if (!Auth::attempt($attr)) {
            return response()->json([
                'message' => 'Login information is invalid.'
              ], 401);
        }

        return response()->json([
            'token' => auth()->user()->createToken('API Token')->plainTextToken,
            'user'  => $request->user()

        ]);
    }

    public function signout()
    {
        auth()->user()->tokens()->delete();
        return [
            'message' => 'Tokens Revoked'
        ];
    }
}

