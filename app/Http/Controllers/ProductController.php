<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateProductRequest;
use App\Http\Resources\Products\ProductCollection;
use App\Http\Resources\Products\ProductResource;
use App\Services\ProductService;
use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\StoreProductRequest;

class ProductController extends Controller
{
    const PAGINATE_COUNT = 9;
    private $service;

    public function __construct(ProductService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $products = $this->service->index(self::PAGINATE_COUNT);
        return new ProductCollection($products);
    }

//    public function indexAllProducts() {
//        $allProducts = $this->service->indexAllProducts();
//        return new ProductCollection($allProducts);
//    }

    public function store(StoreProductRequest $request)
    {
        $product = $this->service->store($request);
        return new ProductResource($product);
    }

    public function update(UpdateProductRequest $request, Product $product)
    {
        $product = $this->service->update($request, $product);
        return new ProductResource($product);
    }

    public function destroy(Product $product)
    {
        $product->delete();
        return $product;
    }







}
