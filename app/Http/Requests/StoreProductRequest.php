<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|min:3|max:255',
            'price' => 'required|numeric',
            'size' => 'required',
            'photo' => 'nullable|min:10|max:255',
            'description' => 'nullable|min:10|max:255',
        ];
    }

}
