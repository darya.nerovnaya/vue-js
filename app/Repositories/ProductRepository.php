<?php

namespace App\Repositories;

use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Product;

class ProductRepository
{
    protected $product;

    private function query()
    {
        return Product::query();
    }

    public function index($length = 9)
    {
        return $this->query()->paginate($length);
    }

    public function indexAllProducts()
    {
        return $this->query()->get();
    }

    public function store(StoreProductRequest $request)
    {
        $data = $request->only(['name', 'price', 'size', 'photo', 'description']);
        return Product::create($data);

    }

    public function update(UpdateProductRequest $request, Product $product)
    {
        $data = $request->only(['name', 'price', 'size', 'photo', 'description']);
        $product->update($data);
        return $product;
    }

}


