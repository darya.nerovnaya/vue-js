<?php

namespace App\Services;

use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Product;
use App\Repositories\ProductRepository;

class ProductService
{
    protected $productRepository;
    /**
     * @var ProductRepository
     */

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function index($length = 9)
    {
        return $this->productRepository->index($length);
    }

    public function indexAllProducts() {
        return $this->productRepository->indexAllProducts();
    }

    public function store(StoreProductRequest $request)
    {
        return $this->productRepository->store($request); // сохранить запись в БД
    }

    public function update(UpdateProductRequest $request, Product $product)
    {
        return $this->productRepository->update($request,  $product); // изменить запись в БД
    }
}


