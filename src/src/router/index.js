import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
import store from '@/store';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
    {
        path: '/products',
        name: 'products',
        component: () => import('../views/ProductsView.vue'),
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/products/create',
        name: 'create',
        component: () => import('../components/products/CreateProduct.vue'),
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/products/edit/:id',
        name: 'edit',
        component: () => import('../components/products/EditProduct.vue'),
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/login',
        name: 'login',
        component: () => import('../views/Auth.vue'),
    },
    {
        path: '/basket',
        name: 'basket',
        component: () => import('../components/products/Basket.vue'),
        props: true,
    },
]

const router = new VueRouter({
  mode: 'history',
  routes
})

router.beforeEach(async (to, from, next) => {
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
    let user = await store.getters['auth/user'];
    if (requiresAuth && !user) {
        next('/');
    } else {
        next();
    }
})

export default router
