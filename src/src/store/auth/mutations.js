export default {
    setUserData: (state, user) => {
        state.userData = user;
    },
    setApiToken: (state, token) => {
        localStorage.removeItem("authToken");
        if(token) {
            state.apiToken = token;
            localStorage.setItem('authToken', token);
        }
    },
}
