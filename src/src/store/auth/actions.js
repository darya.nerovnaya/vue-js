import {axiosInstance} from "@/services/api.js";

export default {
    sendLoginRequest({ commit }, data) {

        commit("setErrors", {}, { root: true });
        return axiosInstance.post('signin', data)
            .then(response => {
                commit("setUserData", response.data.user);
                commit("setApiToken", response.data.token);
            }).catch(err => {
                console.log(err)
            });
    },
    sendRegisterRequest({ commit }, data) {
        commit("setErrors", {}, { root: true });
        return axiosInstance.post("create-account", data)
            .then(response => {
                commit("setUserData", response.data.user);
                commit("setApiToken", response.data.token);
            })
            .catch(err => console.log(err));
    },
    sendLogoutRequest({ commit }) {
        return axiosInstance.post("sign-out").then(() => {
            commit("setUserData", null);
            commit("setApiToken", null);
        });
    },
}
