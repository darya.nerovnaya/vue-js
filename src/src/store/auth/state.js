import getters from "@/store/auth/getters.js";
import mutations from "@/store/auth/mutations.js";
import actions from "@/store/auth/actions.js";

export default {
    namespaced: true,
    state: {
        userData: null,
        apiToken: '',
    },
    getters,
    mutations,
    actions,
}
