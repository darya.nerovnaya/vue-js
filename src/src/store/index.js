import Vue from 'vue'
import Vuex from 'vuex'
import {axiosInstance} from "../services/api.js"
import auth from "@/store/auth/state.js"
import createPersistedState from "vuex-persistedstate"

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
      products: [],
      productsMeta: null,
      page: 1,
      total: 0,
      errors: [],
      searchValue: '',
      basket: [],
  },
  getters: {
      getProducts: (state) => state.products,
      getProductsMeta: (state) => state.productsMeta,
      getProduct: (state) => (id) => state.products.find((product) => product.id === id),
      errors: state => state.errors,
      getSearchValue: (state) => state.searchValue,
      getBasket: (state) => state.basket,
  },

  mutations: {
      getProducts(state, data) {
          state.products = data;
      },
      getProductsMeta(state, data) {
          state.productsMeta = data;
      },
      setErrors: (state, errors) => state.errors = errors,
      setSearchValue: (state, value) => {
          state.searchValue = value;
      },
      setBasket: (state, product) => {
          if(state.basket.length) {
            let isProductExists = false;
            state.basket.forEach(function (item) {
                if (item.id === product.id) {
                    isProductExists = true;
                    item.quantity++;
                }
            })
            if(!isProductExists){
                state.basket.push(product);
            }
          } else {
              state.basket.push(product);
          }
      },
      removeFromBasket: (state, index) => {
          state.basket.splice(index, 1);
      },
      increment: (state, index) => {
          state.basket[index].quantity++;
      },
      decrement: (state, index) => {
          if(state.basket[index].quantity > 1) {
              state.basket[index].quantity--;
          }

      },
  },
  actions: {
      getProducts({commit}, pageNumber) {
          console.log('getProducts', localStorage.getItem("authToken"));
          return axiosInstance.get(`/product?page=${pageNumber}`)
                      .then((res) => {
                          commit('getProducts', res.data.data);
                          commit('getProductsMeta', res.data.meta);
                      })
      },

      createProduct({dispatch}, product) {
          return axiosInstance.post('/product', product)
              .then((resp) => {
                  dispatch('getProducts')
              })
      },

      updateProduct({dispatch}, product) {
          return axiosInstance.put(`/product/${product.id}`, product)
              .then((resp) => {
                  console.log('updateProduct', resp)
              })
      },

      deleteProduct({dispatch}, product) {
          return axiosInstance.delete(`product/${product.id}`)
              .then((resp) => {
                  dispatch('getProducts');
              })
      },

      setSearchValue({commit}, value){
          commit('setSearchValue', value)
      },

      addToBasket({commit}, product){
          commit('setBasket', product)
      },
      deleteFromBasket({commit}, index) {
          commit('removeFromBasket', index)
      },

      incrementBasketItem({commit}, index) {
          commit('increment', index)
      },
      decrementBasketItem({commit}, index) {
          commit('decrement', index)
      }
  },

    modules: {
      auth,
  },
    plugins: [createPersistedState()],
})
