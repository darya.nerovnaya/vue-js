import {validationMixin} from 'vuelidate';
import {required, minLength, numeric} from 'vuelidate/lib/validators';

export default {
    mixins: [validationMixin],
    validations: {
        getProduct: {
            name: {required, minLength: minLength(5)},
            price: {required, numeric},
            size: {required, numeric},
            photo: {required},
            description: {required, minLength: minLength(15)},
        }
    },
    computed: {
        nameErrors() {
            const errors = [];
            if (!this.$v.getProduct.name.required) {
                errors.push('Обязательно для заполнения')
            }
            if (!this.$v.getProduct.name.minLength) {
                errors.push('Название слишком короткое')
            }
            return errors;
        },
        sizeErrors() {
            const errors = [];
            if (!this.$v.getProduct.size.required) {
                errors.push('Обязательно для заполнения')
            }
            if (!this.$v.getProduct.size.numeric) {
                errors.push('Размер должен быть числом')
            }
            return errors;
        },
        priceErrors() {
            const errors = [];
            if (!this.$v.getProduct.price.required) {
                errors.push('Обязательно для заполнения')
            }
            if (!this.$v.getProduct.price.numeric) {
                errors.push('Цена должна быть числом')
            }
            return errors;
        },
        photoErrors() {
            const errors = [];
            if (!this.$v.getProduct.photo.required) {
                errors.push('Обязательно для заполнения')
            }
            return errors;
        },
        descriptionErrors() {
            const errors = [];
            if (!this.$v.getProduct.description.required) {
                errors.push('Обязательно для заполнения')
            }
            if (!this.$v.getProduct.description.minLength) {
                errors.push('Описание слишком короткое')
            }
            return errors;
        },
    },
}
