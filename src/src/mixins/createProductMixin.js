export default {
    data() {
        return {
            getProduct: {
                name: null,
                price: null,
                size: null,
                description: null,
                url: null,
            }
        }
    },
}
