import {validationMixin} from 'vuelidate';
import {required, minLength, numeric, email} from 'vuelidate/lib/validators';

export default {
    mixins: [validationMixin],
    validations: {
        details: {
            name: {required, minLength: minLength(5)},
            email: {required, email},
            password: {required, minLength: minLength(8)},
        }
    },
    computed: {
        nameErrors() {
            const errors = [];
            if (!this.$v.details.name.required) {
                errors.push("Обов'язково для заповнення")
            }
            if (!this.$v.details.name.minLength) {
                errors.push("Им'я занадто коротке")
            }
            return errors;
        },
        emailErrors() {
            const errors = [];
            if (!this.$v.details.email.required) {
                errors.push("Обов'язково для заповнення")
            }
            if (!this.$v.details.email.email) {
                errors.push('Невалідний email')
            }
            return errors;
        },
        passwordErrors() {
            const errors = [];
            if (!this.$v.details.password.required) {
                errors.push("Обов'язково для заповнення")
            }
            if (!this.$v.details.password.minLength) {
                errors.push("Пароль занадто короткий")
            }
            return errors;
        },

    },
}
