import axios from "axios";

const config = {
    withCredentials: true,
    baseURL: 'http://127.0.0.1:8000/api/',
    headers: {
        "content-type": "application/json",
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, PATCH, DELETE, OPTIONS',
    },
}
const apiToken = localStorage.getItem("authToken");
if (apiToken) {
    config.headers.Authorization = `Bearer ${apiToken}`;
}
console.log(config);
const axiosInstance = axios.create(config);

export {
    axiosInstance
}
