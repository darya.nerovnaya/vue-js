import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap";
import 'material-design-icons-iconfont'

Vue.config.productionTip = false
Vue.directive('focus', {
    inserted: function (el, binding) {
        el.focus();
    }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
